﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class safe_sice : MonoBehaviour {

    private bool touch_ball;
    private int safe_counter;

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.name == "ball")
        {
            touch_ball = true;
            safe_counter++;
        }
    }
    private void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.name == "ball")
        {
            touch_ball = false;
            safe_counter++;
        }
    }

    // Update is called once per frame
    void Update () {
        if (touch_ball)
        {
            if (safe_counter != 3)
            {
                transform.localScale -= new Vector3(0.1F, 0, 0);
            }
            else if (safe_counter == 3)
            {
                Destroy(this);
            }           
        }       
    }
}
