﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class push_floor : MonoBehaviour {

    public int number_name;
    public Light light_1;
    public Light light_2;
    public Light light_3;
    private int tempo;


    void Start()
    {
        light_1.enabled = false;
        light_2.enabled = false;
        light_3.enabled = false;

        if (this.name == "1")
        {
            number_name = 1;
        }
        else if (this.name == "2")
        {
            number_name = 2;
        }
    }

    void Update()
    {
        tempo++;
        if (tempo == 30)
        {
            light_1.enabled = true;
        }
        if (tempo == 60)
        {
            light_2.enabled = true;
        }
        if (tempo == 90)
        {
            light_3.enabled = true;
        }
        if (tempo == 100)
        {
            light_1.enabled = false;
            light_2.enabled = false;
            light_3.enabled = false;
            tempo = 0;
        }
    }

}
