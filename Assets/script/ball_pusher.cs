﻿using UnityEngine;
using System.Collections;

public class ball_pusher : MonoBehaviour {

    private bool retract;
    public bool release;
    public ball_ia ball;
    private int tempo = 0;
    public int seconds = 0;
    public Light light_1;
    public Light light_2;
    public Light light_3;
    public Light light_4;


    void Start()
    {
        light_1.enabled = false;
        light_2.enabled = false;
        light_3.enabled = false;
        light_4.enabled = false;
    }




    // Update is called once per frame
    void Update () {
        retract = Input.GetButton("retract");
        release = Input.GetButtonUp("retract");

        if (retract && ball.touch_pusher)
        {
            tempo++;

            if (seconds == 1)
            {
                light_1.enabled = true;
            }
            if (seconds == 2)
            {
                light_2.enabled = true;
            }
            if (seconds == 3)
            {
                light_3.enabled = true;
            }
            if (seconds == 4)
            {
                light_4.enabled = true;
            }

            if (tempo == 60)
            {
                seconds++;
                tempo = 0;
                if (seconds >= 4)
                {
                    seconds = 4;
                }
            }
        }       
	}
}
