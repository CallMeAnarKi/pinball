﻿using UnityEngine;
using System.Collections;

public class bumper_ia : MonoBehaviour {

    private bool touch_ball;
    public Light bump_light;

    private Vector3 bumper_tranform;

    void Start()
    {
        bumper_tranform = transform.localScale;
        bump_light.enabled = false;
    }


    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.name == "ball")
        {
            touch_ball = true;
            bump_light.enabled = true;
        }
    }
    private void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.name == "ball")
        {
            touch_ball = false;
            bump_light.enabled = false;
        }
    }
    void Update()
    {
        if (touch_ball)
        {
            transform.localScale = bumper_tranform + new Vector3(0.2f,0,0.2f);
        }
        else if (!touch_ball)
        {
            transform.localScale = bumper_tranform;
        }
    }
}
