﻿using UnityEngine;
using System.Collections;

public class ball_ia : MonoBehaviour {

    private Rigidbody ball_rb;
    public bool touch_pusher;
    public bool touch_bumper;
    public bool touch_floor;
    public ball_pusher ball_pusher;
    public push_floor push_floor;
    private float ball_vel;
    public int puntos;

    void Start()
    {
        ball_rb = GetComponent<Rigidbody>();
    }

private void OnCollisionEnter (Collision coll)
    {
        if (coll.gameObject.name == "pusher")
        {
            touch_pusher = true;
        }
        if (coll.gameObject.tag == "Bumper")
        {
            puntos = puntos + 100;
            if (ball_rb.velocity.z <= -10f || ball_rb.velocity.z >= 10f)
            {
                ball_rb.AddForce(0, 0, 0, ForceMode.Impulse);
            }
            else
            {
                ball_rb.AddForce(0, -ball_vel, 0, ForceMode.Impulse);
            }
            
        }
        if (coll.gameObject.tag == "Push")
        {
            touch_floor = true;
        }
    }
    private void OnCollisionExit(Collision coll)
    {
        if (coll.gameObject.name == "pusher")
        {
            touch_pusher = false;
            ball_pusher.light_1.enabled = false;
            ball_pusher.light_2.enabled = false;
            ball_pusher.light_3.enabled = false;
            ball_pusher.light_4.enabled = false;
        }
        if (coll.gameObject.tag == "Bumper")
        {
            touch_bumper = false;
        }
        if (coll.gameObject.tag == "Push")
        {
            touch_floor = false; 
        }
    }

    void Update()
    {
        ball_vel = ball_rb.velocity.y;
        Debug.Log(puntos);
        if (ball_pusher.release && touch_pusher)
        {
            switch (ball_pusher.seconds)
            {               
                case 1:                   
                    ball_rb.AddForce(0, 6,0, ForceMode.Impulse);
                    ball_pusher.seconds = 0;
                    break;
                case 2:
                    ball_pusher.light_2.enabled = true;
                    ball_rb.AddForce(0, 10,0, ForceMode.Impulse);
                    ball_pusher.seconds = 0;
                    break;
                case 3:
                    ball_pusher.light_3.enabled = true;
                    ball_rb.AddForce(0,15,0, ForceMode.Impulse);
                    ball_pusher.seconds = 0;
                    break;
                case 4:
                    ball_pusher.light_4.enabled = true;
                    ball_rb.AddForce(0, 22,0, ForceMode.Impulse);
                    ball_pusher.seconds = 0;
                    break;
            }            
        }
        if (touch_floor)
        {
            switch (push_floor.number_name)
            {
                case 1:                    
                    ball_rb.AddForce(0.5f, 1, 0, ForceMode.Impulse);
                    break;
                case 2:
                    ball_rb.AddForce(-0.5f, 1, 0, ForceMode.Impulse);
                    break;
            }
        }
    }
}
