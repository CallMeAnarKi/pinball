﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class diglet_temp : MonoBehaviour {

    private float pos_x;
    private float pos_y;
    private float pos_z;
    public int timer;


    // Update is called once per frame
    void Update() {
        pos_x = transform.position.x;
        pos_y = transform.position.y;
        pos_z = transform.position.z;
        timer++;

        if (timer >= 60)
        {
            this.transform.position = new Vector3(pos_x, pos_y, pos_z - Time.deltaTime);
        }
        if (timer >= 140)
        {
            this.transform.position = new Vector3(pos_x, pos_y, pos_z + Time.deltaTime);
        }
        if (timer == 220)
        {
            timer = 60;
        }


    }
}
