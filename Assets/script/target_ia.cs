﻿using UnityEngine;
using System.Collections;

public class target_ia : MonoBehaviour {

    public counter_target_down counter;
    private float actual_pos_x;
    private float actual_pos_y;
    private float actual_pos_z;
    //public Light target_light;


    void Start()
    {
        actual_pos_x = transform.position.x;
        actual_pos_y = transform.position.y;
        actual_pos_z = transform.position.z;
    }

    private void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.name == "ball")
        {
            Debug.Log("down");
            transform.position = new Vector3(actual_pos_x, actual_pos_y, actual_pos_z+2);
            counter.counter_target++;
        }
    }


    void Update()
    {
        if (counter.target_up)
        {
            Debug.Log("up");
            transform.position = new Vector3(actual_pos_x, actual_pos_y, actual_pos_z);
        }
    }
}
