﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flipper_right : MonoBehaviour {

    private float force_hit = 10000;
    private float spring_damper = 150;

    HingeJoint hinge;

    // Use this for initialization
    void Start()
    {
        hinge = GetComponent<HingeJoint>();
    }

    // Update is called once per frame
    void Update()
    {
        JointSpring spring = new JointSpring();
        spring.spring = force_hit;
        spring.damper = spring_damper;

        if (Input.GetButton("flipper_right"))
        {
            spring.targetPosition = 0;
        }
        else
        {
            spring.targetPosition = -65;
        }
        hinge.spring = spring;
    }
}
