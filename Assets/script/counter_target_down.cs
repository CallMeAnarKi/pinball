﻿using UnityEngine;
using System.Collections;

public class counter_target_down : MonoBehaviour {

    public target_ia target_ia;
    public ball_ia ball_ia;
    public int counter_target = 0;
    public bool target_up;
    private int tempo = 0;

	// Update is called once per frame
	void Update () {
	    if (counter_target == 3)
        {
            tempo++;
            if (tempo == 120)
            {
                target_up = true;
            }
            if (tempo == 125)
            {
                ball_ia.puntos = ball_ia.puntos * 3;
                tempo = 0;
                counter_target = 0;
                target_up = false;
            }           
        }
	}
}
